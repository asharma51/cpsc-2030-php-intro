<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Jekyll v3.8.5">
  <title>Welcome</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="index.css">

  <!-- Custom styles for this template -->
  <link href="form-validation.css" rel="stylesheet">
</head>

<body class="bg-light">

  <div class="container">
    <div class="py-5 text-center">
      <img class="d-block mx-auto mb-4" src="bootstrap-solid.svg" alt="" width="72" height="72">
      <h2>Welcome</h2>     
      <a href="index1.php" class="button c1"><b>Back To Home</b></a>

      <p class="lead"> Please complete the sign up form below</p>
    </div>

    <form action="index.php" method="post" class="needs-validation" novalidate>

      <div class="col-md-12">
        <div class="row">
          
          <div class="col-md-6 mb-3">
            <label for="firstName" class="text">First name</label>
            <input type="text" class="form-control" name="firstName" id="firstName" placeholder="" value="" required>
            <div class="invalid-feedback">
              Valid first name is required.
            </div>
          </div>
          
          <div class="col-md-6 mb-3">
            <label for="lastName" class="text">Last name</label>
            <input type="text" class="form-control" name="lastName" id="lastName" placeholder="" value="" required>
            <div class="invalid-feedback">
              Valid last name is required.
            </div>
          </div>
          
          <div class="col-md-12 mb-3">
            <label for="email" class="text">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="" value="" required>
            <div class="invalid-feedback">
              Please Enter a valid email address
            </div>
          </div>
          
           
             
          <div class="col-md-6 mb-3">
              <label for="username" class="text">Username</label>
              <input type="text" class="form-control" name="username" id="username" placeholder="" value="" required>
              <div class="invalid-feedback">
                  Valid username required.
              </div>
          </div>
                  
          <div class="col-md-6 mb-3">
              <label for="password" class="text">Password</label>
              <input type="password" class="form-control" name="password" id="password" placeholder="" value="" required>
              <div class="invalid-feedback">
                  Valid password required.
              </div>
          </div>
                  
                  
          <div class="col-md-6 mb-3">
            <label for="password" class="text"> Confirm Password</label>
              <input type="password" class="form-control" name="cpassword" id="cpassword" placeholder="" value="" required>
              <div class="invalid-feedback">
                 Valid password required.
              </div>
          </div>
                  
          <div class="col-md-6 mb-3">
              <label for="state" class="text">Select Provience</label>
              <select class="custom-select d-block w-100" id="state" name ="state" onclick="myFunction()" >
                  <option>Select your provience</option>
                  <option>Ontario</option>
                  <option>Qubec</option>
                  <option>British Columbia</option>
                  <option>Alberta</option>
                  <option>Manitoba</option>
                  <option>Saskatchewan</option>         
                  <option>Nova Scotia</option>
                  <option>New Brunswick</option>
                  <option>Newfoundland</option>
                  <option>Prince Edward Island</option>
                  <option>Northwest Territory</option>
                  <option>Yukon</option>
                  <option>Nunavut</option>
                </select>
                <div class="invalid-feedback">
                      Valid state required.
                </div>
          </div>
          

          <button class="btn btn-primary btn-lg btn-block"   type="submit">Submit</button>
          
      </div>
  </div>
  </form>
  </div>
  <script> 
    function myFunction(){
        var p1 = document.getElementById("password").value;
         var p2 = document.getElementById("cpassword").value;
         if(p1 != p2){
             alert("Password Dont Match");
             document.getElementById("cpassword").value = "";
         }
    }
  </script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="form-validation.js"></script>
</body>

</html>
