<!doctype html>
<html lang="en">
    
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Best Mortgage Suppliers</title>
   
  </head>
  
  <body class="container">
    <div class="header">
        <strong>Best Mortgages </strong>
    </div>
    <div class= "nav1">
    <nav>
        <ul>
          <li> <a href="index1.php" class="button b"><strong>Home</strong></a> </li>
          <li> <a href="calculator.html" class="button b"><strong>Calculations</strong></a></li>
          <li> <a href="signup.php" class="button b"><strong>Sign Up</strong></a></li>
          <li> <a href="login.php" class="button b"><strong>Log In</strong></a></li>
          <li> <a href="documentation.html" class="button b"><strong>Documentation</strong></a></li>
        </ul>
    </nav>
    </div>
    <div id="p1">
    
        <?php
           $nl = "<br>";
           $space = " ";
           $homePrice  = $_POST["homePrice"];
           $downPayment = $_POST["downPay"];
           
           $price = $homePrice -$downPayment;
           $interest = $_POST["rate"];
          
          
          if(isset($_POST['period']))
           {
            echo "Your time period : ".$_POST['period'].$nl;
            if($_POST['period'] == "5 Years"){
              $months = 5*12;
            }
            elseif($_POST['period'] == "10 Years"){
              $months = 10*12;
            }
            elseif($_POST['period'] == "15 Years"){
              $months = 15*12;
            }
            elseif($_POST['period'] == "20 Years"){
              $months = 20*12;
            }
            else{
               $months = 25*12;
            }
          }
          
          $rate = $price*($interest/100);
          $price = $price + $rate;
          if(isset($_POST['pay']))
           {
            echo "Your period of payment : ".$_POST['pay'].$nl;
            if($_POST['pay'] == "Monthly"){
              $payment = $price/$months;
            }
            elseif($_POST['pay'] == "Bi weekly"){
              $payment = $price/($months*2);
            }
            
            else{
              $payment = $price/($months*4);
            }
          }
          echo "According to your given interest rate of ".$_POST['rate']."%".$nl;
          if(isset($_POST['submit'])){
           echo "Your Total Price ".$_POST['pay']." will be $". $payment;
          }
         
        ?>
        
    </div>
    <a href="signup.php" class="button a1"><strong>Learn More About Mortgages</strong></a>
  </body>
  
 </html>